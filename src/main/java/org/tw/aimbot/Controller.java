/*
 * Copyright (C) 2020 Christopher Towner <christopher.allen.towner@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tw.aimbot;

import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.concurrent.WorkerStateEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;
import javafx.scene.transform.Rotate;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;

/**
 * Scene controller.
 *
 * @author Christopher Towner <christopher.allen.towner@gmail.com>
 */
public class Controller implements Initializable {

    static final Logger LOG = Logger.getLogger(Main.class.getName());

    @FXML
    private AnchorPane pane;

    private ShipFinder finder;

    private List<Label> labels;
    private boolean showLabels = false;

    private List<Line> guides;
    private boolean showGuides = false;

    private Line sight;
    private Rotate sightRotation;
    private boolean showSights = false;

    private static final double ANGLE_PERCISION = 4.0;
    private boolean aimNearest = false;
    private long timestamp;
    private static final long INPUT_DELAY_MS = 40;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        labels = new LinkedList<>();
        guides = new LinkedList<>();
        sight = new Line();
        timestamp = System.currentTimeMillis();

        //setup our finder service and start
        finder = new ShipFinder();
        finder.setOnSucceeded((WorkerStateEvent t) -> {
            List<Ship> ships = (List<Ship>) t.getSource().getValue();

            //show debug labels
            if (showLabels) {
                pane.getChildren().removeAll(labels);
                labels.clear();

                Label label;
                for (Ship ship : ships) {
                    label = new Label();
                    label.setText(ship.toString());
                    label.relocate(ship.x, ship.y);
                    labels.add(label);
                }

                pane.getChildren().addAll(labels);
            }

            //show guides
            if (showGuides) {
                pane.getChildren().removeAll(guides);
                guides.clear();

                Line line;
                for (Ship ship : ships) {
                    if (ship.enemy) {
                        //FIX hardcoded to center pixel for res 1600x900
                        line = new Line(802, 475, ship.x, ship.y);
                        line.getStyleClass().add("enemy-guide");
                        guides.add(line);
                    }
                }

                pane.getChildren().addAll(guides);
            }

            if (showSights) {
                if (ships != null && !ships.isEmpty()) {
                    Ship ship = ships.get(0);
                    if (ship.self) {
                        LOG.log(Level.FINEST, "sight: {0}", ship.rotation);
                        sightRotation.angleProperty().setValue(-ship.rotation);
                    }
                }
            }

            if (aimNearest) {
                if (ships != null && !ships.isEmpty()) {
                    Ship own = ships.get(0);

                    double closestAngel = 360;
                    boolean moveLeft = false;

                    for (Ship ship : ships) {
                        if (!ship.enemy) {
                            continue;
                        }

                        double target = Ship.findAngle(own.x, own.y, ship.x, ship.y);

                        //if close enough, lets fire
                        if (Math.abs(target - own.rotation) <= ANGLE_PERCISION) {
                            pressAndReleaseKey(KeyCode.CONTROL);
                            break;
                        } else {
                            //determine closest ship (by angel to fire at)
                            double diff = Math.abs(target - own.rotation);
                            if (diff >= 180) {
                                if (target + own.rotation >= 360) {
                                    double angel = Math.abs(target - own.rotation - 360);
                                    if (angel < closestAngel) {
                                        closestAngel = angel;
                                        moveLeft = target < own.rotation;
                                    }
                                } else {
                                    double angel = target + own.rotation - 180;
                                    if (angel < closestAngel) {
                                        closestAngel = angel;
                                        moveLeft = target < own.rotation;
                                    }
                                }
                            } else {
                                if (diff < closestAngel) {
                                    closestAngel = diff;
                                    moveLeft = target > own.rotation;
                                }
                            }
                        }
                    }

                    //adjust rotation to nearest ship (angel)
                    if (closestAngel != 360) {
                        if (moveLeft) {
                            pressKey(KeyCode.LEFT);
                        } else {
                            pressKey(KeyCode.RIGHT);
                        }
                    } else if (lastPress != null) {
                        Main.getRobot().keyRelease(lastPress.getCode());
                        lastPress = null;
                    }
                }
            }
        });
        finder.start();
    }

    @FXML
    protected void reload() {
        finder.cancel();
        Main.load();
    }

    @FXML
    protected void showLabels() {
        showLabels = !showLabels;
        pane.getChildren().removeAll(labels);
    }

    @FXML
    protected void showGuides() {
        showGuides = !showGuides;
        pane.getChildren().removeAll(guides);
    }

    @FXML
    protected void showSights() {
        showSights = !showSights;
        pane.getChildren().removeAll(sight);
        if (showSights) {
            //FIX harcoded to 1600x900 with 16px halfship size with 2px offset
            //ie, 802 + 16 + 2
            sight = new Line(820, 475, 1606, 475);
            sight.getStyleClass().add("self-sight");

            sightRotation = new Rotate();
            sightRotation.setPivotX(802);
            sightRotation.setPivotY(475);
            sight.getTransforms().add(sightRotation);

            pane.getChildren().add(sight);
        }
    }

    @FXML
    protected void aimNearest() {
        aimNearest = !aimNearest;
    }

    @FXML
    protected void screenshot() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Screenshot");
        fileChooser.setInitialFileName("screenshot.bmp");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        File file = fileChooser.showSaveDialog(Main.getStage());
        if (file != null) {
            try {
                BufferedImage image = Main.getRobot().createScreenCapture(Main.getRECT().toRectangle());
                ImageIO.write(image, "bmp", file);
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Unable to save screenshot", ex);
            }
        }
    }

    @FXML
    protected void close() {
        finder.cancel();
        Platform.exit();
    }

    public Line getLine(int x, int y, double degrees) {
        Point2D end = getEndPoint(x, y, degrees);
        return new Line(x, y, end.getX(), end.getY());
    }

    public Point2D getEndPoint(int x, int y, double degrees) {
        return getEndPoint(x, y, degrees, Math.hypot(pane.getWidth(), pane.getHeight()));
    }

    public Point2D getEndPoint(int x, int y, double degrees, double length) {
        double radians = Math.toRadians(length);
        double endX = x + length * Math.cos(radians);
        double endY = y - length * Math.sin(radians);
        return new Point2D.Double(endX, endY);
    }

    private KeyCode lastPress;
    
    /**
     * Press key if delay has been past.
     *
     * @param keyCode
     */
    private void pressKey(KeyCode keyCode) {
        if (System.currentTimeMillis() - timestamp > INPUT_DELAY_MS) {
            if (lastPress != keyCode && lastPress != null) {
                Main.getRobot().keyRelease(lastPress.getCode());
            }
            Main.getRobot().keyPress(keyCode.getCode());
            lastPress = keyCode;
            timestamp = System.currentTimeMillis();
        }
    }
    
    private void pressAndReleaseKey(KeyCode keyCode) {
        if (System.currentTimeMillis() - timestamp > INPUT_DELAY_MS) {
            Main.getRobot().keyPress(keyCode.getCode());
            Main.getRobot().keyRelease(keyCode.getCode());
            timestamp = System.currentTimeMillis();
        }
    }
}
