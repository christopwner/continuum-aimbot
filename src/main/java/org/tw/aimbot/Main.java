/*
 * Copyright (C) 2020 Christopher Towner <christopher.allen.towner@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tw.aimbot;

import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;
import java.awt.AWTException;
import java.awt.Robot;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.paint.Color;
import javafx.stage.StageStyle;

/**
 * Main class for application.
 */
public class Main extends Application {

    private static final Logger LOG;

    static {
        //load logging properties
        System.setProperty("java.util.logging.config.file",
                Main.class.getResource("logging.properties").getFile());
        LOG = Logger.getLogger(Main.class.getName());
    }

    private static Stage stage;
    private static Scene scene;
    private static WinDef.HWND hwnd;
    private static WinDef.RECT rect;
    private static Robot robot;

    @Override
    public void start(Stage stage) {
        Main.stage = stage;

        //try to get robot immediately, close app if unsuccessful
        try {
            robot = new Robot();
            //robot.setAutoDelay(200);
            //robot.setAutoWaitForIdle(true);
        } catch (AWTException ex) {
            LOG.log(Level.SEVERE, "Unable to load robot", ex);
            Platform.exit();
        }

        //set transparent window border and attempt to keep on top
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.setAlwaysOnTop(true);

        //find continuum window and setup bounds
        hwnd = User32.INSTANCE.FindWindow(null, "Subspace Continuum");
        if (hwnd == null) {
            LOG.log(Level.SEVERE, "Unable to find Subspace Continuum running");
            Platform.exit();
        } else {
            rect = new WinDef.RECT();
            if (User32.INSTANCE.GetWindowRect(hwnd, rect)) {
                stage.setX(rect.left);
                stage.setY(rect.top);
                int width = rect.right - rect.left;
                int height = rect.bottom - rect.top;
                stage.setWidth(width);
                stage.setHeight(height);
                LOG.log(Level.INFO, "width: {0}, height: {1}", new Object[]{width, height});
            }
        }

        try {
            Parent root = FXMLLoader.load(Main.class.getResource("scene.fxml"));
            root.setStyle("-fx-background-color: transparent");
            scene = new Scene(root);
            scene.setFill(Color.TRANSPARENT);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Unable to load scene.fxml", ex);
            Platform.exit();
        }

        //set stage and show
        stage.setScene(scene);
        stage.show();
    }

    public static void load() {
        try {
            Parent root = FXMLLoader.load(Main.class.getResource("scene.fxml"));
            root.setStyle("-fx-background-color: transparent");
            scene.setRoot(root);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Unable to load scene.fxml", ex);
            Platform.exit();
        }
    }

    public static void main(String[] args) {
        launch();
    }

    public static Stage getStage() {
        return stage;
    }

    public static Scene getScene() {
        return scene;
    }

    public static WinDef.HWND getHWND() {
        return hwnd;
    }

    public static WinDef.RECT getRECT() {
        return rect;
    }

    public static Robot getRobot() {
        return robot;
    }
}
