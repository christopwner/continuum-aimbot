/*
 * Copyright (C) 2020 Christopher Towner <christopher.allen.towner@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tw.aimbot;

import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Ship class contains logic for identifying ships. We create custom graphics
 * for the continuum shipset with embedded pixel data for scraping. The RGBs
 * values are as follows:
 * <ul>
 * <li>r = 0 (lots of reds used so we avoid to prevent conflicts)
 * <li>g = 255 - {@link Ship.Type} ordinal value
 * <li>b = 255 - rot (ie facing right is 255, facing up is 245, facing left 235)
 * </ul>
 *
 * <p>
 *
 *
 * @author Christopher Towner <christopher.allen.towner@gmail.com>
 */
public class Ship {

    private static final Logger LOG = Logger.getLogger(Ship.class.getName());

    public final Type type;
    public final int x;
    public final int y;
    public final int rotation;
    public final boolean enemy;
    public final boolean self;

    public Ship(int x, int y, Type type, int rotation, boolean ally, boolean self) {
        this.x = x;
        this.y = y;
        this.type = type;
        this.rotation = rotation;
        this.enemy = ally;
        this.self = self;
    }

    /**
     * Return the closest angle available to ship in degrees for target.
     *
     * @param x1 source x
     * @param y1 source y
     * @param x2 target x
     * @param y2 target y
     * @return angle between both ships
     */
    public static double findAngle(int x1, int y1, int x2, int y2) {
        final double deltaY = y1 - y2;
        final double deltaX = x2 - x1;
        final double result = Math.toDegrees(Math.atan2(deltaY, deltaX));
        return (result < 0) ? (360d + result) : result;
    }

    @Override
    public String toString() {
        return String.format("t: %s, x: %d, y: %d, r: %d, e: %b", type.name(), x, y, rotation, enemy);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.type);
        hash = 97 * hash + this.x;
        hash = 97 * hash + this.y;
        hash = 97 * hash + this.rotation;
        hash = 97 * hash + (this.enemy ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ship other = (Ship) obj;
        if (this.x != other.x) {
            return false;
        }
        if (this.y != other.y) {
            return false;
        }
        if (this.rotation != other.rotation) {
            return false;
        }
        if (this.enemy != other.enemy) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        return true;
    }

    /**
     * All ship types.
     */
    public static enum Type {
        WARBIRD, JAVELIN, SPIDER, LEVIATHAN, TERRIER, WEASEL, LANCASTER, SHARK;

        /**
         * Determine ship type based on green value.
         *
         * @param green green value of pixel
         * @return type of ship
         */
        public static Type valueOf(int green) {
            int n = 255 - green + 1;
            switch (n) {
                case 1:
                    return WARBIRD;
                case 2:
                    return JAVELIN;
                case 3:
                    return SPIDER;
                case 4:
                    return LEVIATHAN;
                case 5:
                    return TERRIER;
                case 6:
                    return WEASEL;
                case 7:
                    return LANCASTER;
                case 8:
                    return SHARK;
                default:
                    LOG.log(Level.WARNING, "Testing invalid green value: {0}", green);
                    return null;
            }
        }
    };
}
