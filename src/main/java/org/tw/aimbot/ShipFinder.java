/*
 * Copyright (C) 2020 Christopher Towner <christopher.allen.towner@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tw.aimbot;

import java.awt.image.BufferedImage;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;

/**
 * Scheduled service for finding ships.
 *
 * @author Christopher Towner <christopher.allen.towner@gmail.com>
 */
public class ShipFinder extends ScheduledService<List<Ship>> {

    private static final Logger LOG = Logger.getLogger(ShipFinder.class.getName());

    //extents for pixel data
    public static final int SHIP_COUNT = 8;
    public static final int ROT_COUNT = 40;
    public static final int R = 0;
    public static final int G = 255 - SHIP_COUNT;
    public static final int B = 255 - ROT_COUNT;

    //rgbs for friend/foe detection
    private static final int ENEMY_RGB = -3750153;
    private static final int TEAM_RGB = -10662;

    //pixel insets for calc center
    private static final int TOP_PX = 26;
    private static final int BTM_PX = 3;

    private final ListProperty<Ship> ships = new SimpleListProperty();

    public final void setShips(List<Ship> list) {
        ships.set(FXCollections.observableList(list));
    }

    public final List<Ship> getShips() {
        return ships.get();
    }

    public final ListProperty<Ship> shipsProperty() {
        return ships;
    }

    @Override
    protected Task<List<Ship>> createTask() {
        Task<List<Ship>> task = new Task<>() {
            @Override
            protected List<Ship> call() throws Exception {
                BufferedImage image = Main.getRobot().createScreenCapture(Main.getRECT().toRectangle());
                return find(image);
            }
        };
        return task;
    }

    /**
     * Find all ships given an image.
     *
     * @param image buffered image to analyze
     * @return immutable list of ships, first ship is own ship (always center)
     */
    public static List<Ship> find(BufferedImage image) {
        List<Ship> list = new LinkedList<>();

        int centerX = (image.getWidth() / 2) - 1;
        int centerY = ((image.getHeight() - TOP_PX - BTM_PX) / 2) + (TOP_PX - 1);

        int pixel, a, r, g, b;
        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                pixel = image.getRGB(x, y);
                a = (pixel >> 24) & 0xff;
                r = (pixel >> 16) & 0xff;
                g = (pixel >> 8) & 0xff;
                b = (pixel) & 0xff;

                if (r == 0) {
                    if (g >= G && b >= B) {
                        Ship.Type type = Ship.Type.valueOf(g);
                        int rotation = (255 - b) * 9;
                        boolean enemy = false;

                        //could misidentify names with " " in 3rd char
                        //determine if enemy by checking font color of name
                        //also check that we dont read off screen
                        int xoff = x + 31;
                        int yoff = y + 16;
                        search:
                        for (int j = yoff; j < yoff + 9 && j < image.getHeight(); j++) {
                            for (int i = xoff; i < xoff + 7 && i < image.getWidth(); i++) {
                                int rgb = image.getRGB(i, j);
                                if (rgb == ENEMY_RGB) {
                                    enemy = true;
                                    break search;
                                }
                                if (rgb == TEAM_RGB) {
                                    break search;
                                }
                            }
                        }

                        boolean self = (x == centerX && y == centerY);
                        Ship ship = new Ship(x, y, type, rotation, enemy, self);

                        //add ship to start of list if center
                        if (self) {
                            list.add(0, ship);
                        } else {
                            list.add(ship);
                        }
                        LOG.log(Level.FINEST, "found ship {0}", ship);
                    }
                }
            }
        }

        return Collections.unmodifiableList(list);
    }
}
