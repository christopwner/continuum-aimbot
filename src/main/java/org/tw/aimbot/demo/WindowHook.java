/*
 * Copyright (C) 2020 Christopher Towner <christopher.allen.towner@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tw.aimbot.demo;

import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinDef.HMODULE;
import com.sun.jna.platform.win32.WinNT;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.platform.win32.WinUser;
import com.sun.jna.platform.win32.WinUser.MSG;
import com.sun.jna.platform.win32.WinUser.WinEventProc;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Sample implementation of a low-level keyboard hook on W32.
 */
public class WindowHook {

    private static volatile boolean quit;
    private static WinUser.WinEventProc winHook;

    public static void main(String[] args) throws InterruptedException {
        final User32 lib = User32.INSTANCE;
        HMODULE hMod = Kernel32.INSTANCE.GetModuleHandle(null);
        winHook = new WinEventProc() {
            @Override
            public void callback(WinNT.HANDLE hWinEventHook, WinDef.DWORD event, 
                    WinDef.HWND hwnd, WinDef.LONG idObject, WinDef.LONG idChild, 
                    WinDef.DWORD dwEventThread, WinDef.DWORD dwmsEventTime) {
                System.out.println(event);
            }
        };

        HANDLE handle = lib.SetWinEventHook(0, 99, hMod, winHook, 0, 0, 0);
        
        new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(WindowHook.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    lib.UnhookWinEvent(handle);
                    System.exit(0);
                }
            }
        }.start();

        int result;
        MSG msg = new MSG();
        while ((result = lib.GetMessage(msg, null, 0, 0)) != 0) {
            if (result == -1) {
                System.err.println("error in get message");
                break;
            } else {
                System.err.println("got message");
                lib.TranslateMessage(msg);
                lib.DispatchMessage(msg);
            }
        }
        lib.UnhookWinEvent(handle);
    }
}
