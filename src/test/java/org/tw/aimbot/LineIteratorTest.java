/*
 * Copyright (C) 2020 Christopher Towner <christopher.allen.towner@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tw.aimbot;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.junit.jupiter.api.Test;

/**
 * Test for LineIterator.
 *
 * @author Christopher Towner <christopher.allen.towner@gmail.com>
 */
public class LineIteratorTest {

    @Test
    public void testIterator() {
        List<Point2D> list = new ArrayList<>();
        Line2D line = new Line2D.Double(0, 0, 8, 4);
        Point2D current;
        for (Iterator<Point2D> it = new LineIterator(line); it.hasNext();) {
            current = it.next();
            list.add(current);
        }

        System.out.println(list.toString());
//        assertThat(ary.toString(),
//                is("[Point2D.Double[0.0, 0.0], Point2D.Double[1.0, 0.0], "
//                        + "Point2D.Double[2.0, 1.0], Point2D.Double[3.0, 1.0], "
//                        + "Point2D.Double[4.0, 2.0], Point2D.Double[5.0, 2.0], "
//                        + "Point2D.Double[6.0, 3.0], Point2D.Double[7.0, 3.0]]"));
    }

    @Test
    public void testIterator2() {
        List<Point2D> list = new ArrayList<>();
        Line2D line = new Line2D.Double(new Point2D.Double(4, 4), new Point2D.Double(10, 0));
        Point2D current;
        for (Iterator<Point2D> it = new LineIterator(line); it.hasNext();) {
            current = it.next();
            list.add(current);
        }

        System.out.println(list.toString());
//        assertThat(ary.toString(),
//                is("[Point2D.Double[4.0, 4.0], Point2D.Double[5.0, 3.0], Point2D.Double[6.0, 3.0], "
//                        + "Point2D.Double[7.0, 2.0], Point2D.Double[8.0, 1.0], Point2D.Double[9.0, 1.0]]"));
    }

    @Test
    public void testIterator3() {
        List<Point2D> list = new ArrayList<>();
        Line2D line = new Line2D.Double(new Point2D.Double(0, 0), new Point2D.Double(100, 0));
        Point2D current;
        for (Iterator<Point2D> it = new LineIterator(line); it.hasNext();) {
            current = it.next();
            list.add(current);
        }

        System.out.println(list.size());
//        assertThat(ary.size(), is(100));
    }
}
