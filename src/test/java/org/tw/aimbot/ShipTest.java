/*
 * Copyright (C) 2020 Christopher Towner <christopher.allen.towner@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tw.aimbot;

import java.awt.Color;
import java.awt.geom.Point2D;
import org.tw.aimbot.Ship.Type;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.shape.Line;
import javax.imageio.ImageIO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * Test the ship detecting against graphic (gfx) files for Continuum.
 *
 * @author Christopher Towner <christopher.allen.towner@gmail.com>
 */
public class ShipTest {

    private static final Logger LOG = Logger.getLogger(ShipTest.class.getName());

    private static final short SHIP_COUNT = 8;
    private static final short ROT_COUNT = 40;

    @Test
    public void testAllPixelsInGraphicsForConflict() throws URISyntaxException, IOException {
        BufferedImage image; //image to test

        File directory = new File(ShipTest.class.getResource("gfx").toURI());
        for (File f : directory.listFiles()) {
            image = ImageIO.read(f);
            List<Ship> ships = ShipFinder.find(image);

            //assert no ships found in standard graphics set
            assertEquals(0, ships.size(), "Conflict found in " + f);
        }
    }

    @RepeatedTest(SHIP_COUNT)
    public void testTypeValueOf(RepetitionInfo repetitionInfo) {
        int i = repetitionInfo.getCurrentRepetition();
        int g = 255 - i + 1;
        LOG.log(Level.FINE, "testing green: {0} for ship: {1}",
                new Object[]{g, Type.values()[i - 1]});
        assertEquals(Type.values()[i - 1], Type.valueOf(255 - i + 1));
    }

    @ParameterizedTest
    @ValueSource(strings = {"wb00r1600x900.bmp", "ships.bm2"})
    public void testFindShips(String resource) throws IOException {
        BufferedImage image = ImageIO.read(ShipTest.class.getResourceAsStream(resource));

        List<Ship> ships = ShipFinder.find(image);
        assertTrue(ships.size() > 0);

        if (resource.equals("ships.bmp")) {
            assertEquals(SHIP_COUNT, ships.stream().map(s -> s.type).distinct().count());
            assertEquals(SHIP_COUNT * ROT_COUNT, ships.size());
        }
    }

    @Test
    public void testFindAngle() {
        assertEquals(315, Ship.findAngle(1, 1, 2, 2));
        assertEquals(90, Ship.findAngle(2, 2, 2, 1));
    }

    /**
     * Test installed graphics match what we have in our resource folder.
     *
     * @param resource
     * @throws Exception
     */
    @ParameterizedTest
    @ValueSource(strings = {"ships.bm2"})
    public void testGraphics(String resource) throws Exception {
        String path = String.join(File.separator,
                System.getenv("ProgramFiles(X86)"), "Steam", "steamapps",
                "common", "Continuum", "graphics", resource);
        File theirs = new File(path);
        File ours = new File(ShipTest.class.getResource(resource).toURI());

        FileChannel installed = new RandomAccessFile(theirs, "r").getChannel();
        FileChannel required = new RandomAccessFile(ours, "r").getChannel();

        long size = installed.size();
        assertEquals(size, required.size(), "Installed " + resource + " size doesnt match");

        ByteBuffer m1 = installed.map(FileChannel.MapMode.READ_ONLY, 0L, size);
        ByteBuffer m2 = required.map(FileChannel.MapMode.READ_ONLY, 0L, size);
        for (int pos = 0; pos < size; pos++) {
            assertEquals(m1.get(pos), m2.get(pos), "Installed " + resource + " doesn't match at position " + pos);
        }
    }

    @Test
    public void testIsEnemy() throws IOException {
        BufferedImage image = ImageIO.read(ShipTest.class.getResourceAsStream("fr2.bmp"));

        List<Ship> ships = ShipFinder.find(image);

        assertEquals(13, ships.size());
        System.out.println(Color.decode("#c6c6f7").getRGB());
    }

    @Test
    public void testFindRay() {
//        Controller controller = mock(Controller.class);
//        
//        assertEquals(new Point(2, 1), controller.getRay(1, 1, 0, 1));
//        assertEquals(new Point(1, 0), controller.getRay(1, 1, 90, 1));
//        assertEquals(new Point(0, 1), controller.getRay(1, 1, 180, 1));
//        assertEquals(new Point(1, 2), controller.getRay(1, 1, 270, 1));
    }
    
    @Test
    public void testFindCenterShip() throws IOException {
        BufferedImage image = ImageIO.read(ShipTest.class.getResourceAsStream("fr1.bmp"));
        List<Ship> ships = ShipFinder.find(image);
        Ship expected = new Ship(802, 475, Ship.Type.TERRIER, 0, false, true);
        assertEquals(expected, ships.get(0));
    }

}
